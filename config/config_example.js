module.exports = {
    twitch: {
        enable : true,
        username: "the_nick_name_of_the_bot", //it can be your own account
        oauth: "oauth:the_key_associated", 
        prefix: "!",
        channels : [
            "the_channel_to_join"
        ],
        commands_list: { sr, sl }, //available: { sr, sl }, 
        commands_options: {
            slUrl :'your song list url '
        }
    },
    db: {
        host: '127.0.0.1',
        user: 'username_for_database',
        password: 'the_password',
        database: 'the_database_name'
    },
    helloasso: {
        username : 'the_username_provided_by_helloasso',
        key : 'a_very_long_key_provided_by_helloasso',
        campaignid : 'campaign_id_cf_doc'
    },
}