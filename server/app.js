//########################################################################
// Définitions des variables
//########################################################################

//requirements configuration server
const express = require("express");
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const pug = require('pug');
const bodyParser = require('body-parser')
const fetch = require("node-fetch");
const tmi = require('tmi.js')
const datas = require('../config/config.js')
// Define database configuration:
const paramsDB = datas['db']
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: paramsDB['host'],
        user: paramsDB['user'],
        password: paramsDB['password'],
        database: paramsDB['database']
    }
});
// Define twitch configuration:
// Valid commands start with:
const paramsTwitch = datas['twitch']
const commandPrefix = paramsTwitch['prefix']
const srOpenTime = paramsTwitch['commands_options']['srOpenTime']
const opts = {
    identity: {
        username: paramsTwitch['username'],
        password: paramsTwitch['oauth']
    },
    channels: [
        paramsTwitch['channels'][0]
    ]
}
// Configure options for twitch command
const slUrl = paramsTwitch['commands_options']['slUrl']
// These are the commands the bot knows (defined below):
let knownCommands = { sr, sl }

// Define helloasso configuration:
const paramsHelloAsso = datas['helloasso']
const authlogin = paramsHelloAsso['username'] + ':' + paramsHelloAsso['key']
const campaignId = paramsHelloAsso['campaignid']


//variables des dons
let funding = 0;
let lastName = "nobody";
let lastAmount = 0;
let lastDate = "20130201T13:00:00";
var start = Date.now();
// some configurations 
const DONATION = "DONATION";
const TEN_MINUTES = 1000 * 60 * 10;
const PORT = 3000;
const VIP = "vip"
const STANDARD = "standard"
const BADIDRESPONSECODE = 'com.helloasso.api.InvalidIdentifier'

//########################################################################
// Définitions des modéles
//########################################################################

async function manageRequestFromPOST(pseudo, montant) {
    try {
        const data = await knex.select('*').from(STANDARD).where('pseudo', '=', pseudo).groupBy('pseudo')
        if (data.length == 0) {
            const requete = ''
            const useless = await knex(VIP).insert({ pseudo, requete, montant })
        }
        else {
            const useless = await knex(STANDARD).where('id', '=', data[0]['id']).del()
            const requete = data[0]['requete']
            const useless2 = await knex(VIP).insert({ pseudo, requete, montant })
            sendIoBroadcast('serverDelRow', { id: data[0]['id'], table: 'tabloSTD' })
            sendIoBroadcast('newRequest', { id: data[0]['id'], pseudo, requete, table: "vip", montant });
        }
    } catch (error) {
        console.log(error);
    }
}

async function getVIPEmptyRequestByUser(user, table) {
    try {
        const data = await knex.select('*').from(table).where('requete', '=', '').andWhere('pseudo', '=', user).groupBy('pseudo')
        if (data.length == 0) {
            return false;
        }
        else {
            return data
        }
    } catch (error) {
        console.log(error);
    }
}

async function assignRequestToVip(id, requete, pseudo) {
    try {
        const data = await knex(VIP).update('requete', requete).where('id', '=', id)
        let montant = await knex.select('montant').from(VIP).where('id', '=', id)
        montant = montant[0]['montant'];
        await sendIoBroadcast('newRequest', { id, pseudo, requete, table: VIP, montant });
    } catch (error) {
        console.log(error);
    }
}

async function newStandardRequest(pseudo, requete) {
    try {
        const idArray = await knex(STANDARD).insert({ pseudo, requete })
        const id = idArray[0]
        await sendIoBroadcast('newRequest', { id, pseudo, requete, table: STANDARD });
    } catch (error) {
        console.log(error);
    }
}
async function deleteRow(id, status) {
    let table = status === 'tabloVIP' ? VIP : STANDARD
    console.log(table);

    try {
        await knex(table).where('id', '=', id).del()
    } catch (error) {
        console.log(error);
    }
}
async function updateRow(id, status, requete) {
    let table = status === 'tabloVIP' ? VIP : STANDARD
    console.log(table);

    try {
        await knex(table).where('id', '=', id).update({ requete })
    } catch (error) {
        console.log(error);
    }
}

async function initLists() {
    let json
    const vipList = await knex.select('*').from(VIP).where('requete', '!=', '').orderBy('id')
    const standardList = await knex.select('*').from(STANDARD).orderBy('id')
    json = { vip: vipList, standard: standardList }
    return json
}

//########################################################################
// Définitions des fonctions
//########################################################################

async function sendIoBroadcast(type, datas) {
    io.sockets.emit(type, datas)
}
async function manageNewPayment(params) {
    console.log(params);

    try {
        let infos = await getInfos(params.id)
        console.log(infos);
        if (infos === false) {
            console.log("pas de pseudo");
        }
        else {
            params['amount'] = infos['amount']
            params['pseudo'] = infos['pseudo']
            console.log(Date.now());

            console.log(Boolean(Date.now() > 1531920238837));
            const stamp = Date.now()
            if ((stamp > srOpenTime[0]) && (stamp < srOpenTime[1])) {
                console.log("c'est dedans");

                const hasAStandardRequest = await manageRequestFromPOST(params['pseudo'], params['amount'])
            }
            else {
                console.log("c'est pas dedans")
            }
        }
        checkAndUpdate(concat(params));
        sendNotif(true);
    }
    catch (err) {
        console.log("Error: " + err);
    }
}
function setCharAt(str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}


async function getInfos(id) {
    console.log(id);
    try {
        let firstTryId = '0000' + id + '1'
        let firstTryResp = await fetch(`https://${authlogin}@api.helloasso.com/v3/actions/${firstTryId}.json`)
        let firstTryObj = await firstTryResp.json()
        if (firstTryObj['code'] == BADIDRESPONSECODE) {
            console.log("mauvais id au premier coup");
        }
        else {
            console.log("id trouvé au premier coup");
            const customInfos = firstTryObj['custom_infos'][0]
            if (typeof customInfos === undefined) {
                return false
            }
            else {
                const pseudo = (customInfos['value'] || '').toLowerCase()
                const infos = {
                    pseudo,
                    amount: firstTryObj['amount'],
                }
                return infos
            }
        }
        let idWithoutLastNumber = id.substr(-1)
        for (let i = 0; i < 10; i++) {
            idWithoutLastNumber = setCharAt(id, 6, i)
            idWithoutLastNumber = setCharAt(idWithoutLastNumber, 7, '')
            const idQuery = '0000' + idWithoutLastNumber + '1'
            console.log(idQuery);
            const apiUrl = `https://${authlogin}@api.helloasso.com/v3/actions/${idQuery}.json`
            const response = await fetch(apiUrl)
            const result = await response.json()
            if (result['code'] == BADIDRESPONSECODE) {
                console.log("mauvais id");
            }
            else {
                console.log();
                console.log(result);
                const customInfos = result['custom_infos'][0]
                if (typeof customInfos === undefined) {
                    return false
                }
                else {
                    const pseudo = (customInfos['value'] || '').toLowerCase()
                    const infos = {
                        pseudo,
                        amount: result['amount'],
                    }
                    return infos
                }
            }
        }
        return false
    }
    catch (err) {
        console.log("Error: " + err.message);
    }

}



async function init() {
    start = Date.now();
    funding = 0;
    lastName = "nobody";
    lastAmount = 0;
    lastDate = "20130201T13:00:00";
    try {
        let repfindlast = await fetch('https://' + authlogin + '@api.helloasso.com/v3/campaigns/' + campaignId + '/actions.json?from=2017-01-01T00:00:00&results_per_page=1&page=1')
        let objfindlast = await repfindlast.json()
        console.log(objfindlast["pagination"]['max_page']);
        let replast = await fetch('https://' + authlogin + '@api.helloasso.com/v3/campaigns/' + campaignId + `/actions.json?from=2017-01-01T00:00:00&results_per_page=1&page=${objfindlast["pagination"]['max_page']}`)
        let objlast = await replast.json()
        let action = objlast['resources'][0]
        console.log(action);
        let repstatus = await fetch('https://' + authlogin + '@api.helloasso.com/v3/campaigns/' + campaignId + `.json`)
        let status = await repstatus.json()
        console.log(status['funding']);
        if (action['custom_infos'][0] !== undefined) {
            action['pseudo'] = action['custom_infos'][0]['value'].toLowerCase()
        }
        checkAndUpdate(concat(action))
        funding = status['funding']
        sendNotif(false);
    }
    catch (err) {
        console.log("Error: " + err.message);
    }
    var millis = Date.now() - start;
    console.log("init done in miliseconds : " + Math.floor(millis));

}

function concat({ id, payer_first_name, payer_last_name, first_name = payer_first_name, last_name = payer_last_name, amount, date, pseudo }) {
    console.log("test");
    console.log(pseudo);
    console.log("fin test");
    const name = pseudo
        ? pseudo
        : first_name + " " + last_name.substr(0, 1) + ".";
    console.log("retour nom  de fonction concat: " + name);

    return {
        id,
        name,
        amount,
        date,
    };
}

function checkAndUpdate(don) {
    console.log("check");

    console.log(don);

    lastId = don['id']
    lastName = don['name']
    lastAmount = parseFloat(don['amount'])
    lastDate = don['date']

    console.log(lastName);
    funding = funding + parseFloat(don['amount']);
    funding = Math.round(funding * 100) / 100;
}

function prepareToSend(sound) {
    console.log(lastName);

    const message = {
        funding,
        lastName,
        lastAmount,
        sound,
    }
    return message;
}

function sendNotif(sound) {
    sendIoBroadcast('message', prepareToSend(sound));
}

function intervalFunc() {
    init()
}

//########################################################################
// Programme principal
//########################################################################

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: false }))
server.listen(PORT)

init()
setInterval(intervalFunc, TEN_MINUTES);


// Entry point of webserver
app.get('((/stats)|(/))', (req, res) => {
    res.redirect('stats.html');
})
app.get('/init', (req, res) => {
    res.send('initialisation en cours');
    init()
})
app.get('/stats.html', (req, res) => {
    res.send(pug.renderFile('static/stats.pug', {
        color: req.query.color == "white"
            ? "white"
            : "black"
    }));
})
app.get('/asks.html', (req, res) => {
    res.send(pug.renderFile('static/asks.pug', {
        color: req.query.color == "white"
            ? "white"
            : "black"
    }));
})
app.use(express.static('static'))
// This url will be called when a campaign is created/updated
// use setTimeout to answer as fast as possible
app.post('/campaigns', (req, res) => {

    console.log('POST on /campaigns receive');
    res.send("POST on /campaigns receive")
})

// This url will be called when new payment validated
// use setTimeout to answer as fast as possible
app.post('/payments', (req, res) => {
    setTimeout(() => {
        manageNewPayment(req.body)
    }, 0)
    console.log('POST on /payments receive');
    res.send("POST on /payments receive")


})

//########################################################################
// websocket
//########################################################################

io.on('connection', function (socket) {
    console.log(socket.id)
    socket.emit('message', 'Vous êtes bien connecté !');
    sendNotif(false);

    socket.on('requestConnection', async function (socket) {
        console.log(socket);

        console.log('un utilisateur sur les requetes');
        this.emit('initLists', await initLists())
    })

    socket.on('clientDelRow', function (datas) {
        console.log(datas);
        deleteRow(datas['id'], datas['status'])
        sendIoBroadcast('serverDelRow', { id: datas['id'], table: datas['status'] })
    })

    socket.on('clientUpRow', function (datas) {
        console.log(datas);
        updateRow(datas['id'], datas['status'], datas['requete'])
        sendIoBroadcast('serverUpRow', { id: datas['id'], table: datas['status'], requete: datas['requete'] })
    })
});

//########################################################################
// Définitions des fonctions de chat 
//########################################################################

// Function called when the "sr" command is issued:
async function sr(target, context, params) {
    // think to filter sql injection
    if (params.length) {

        let requete = ''
        for (let i = 0; i < params.length; i++) {
            requete += params[i] + " ";
        }
        requete = requete.substring(0, requete.length - 1);
        const row = await getVIPEmptyRequestByUser(context.username, VIP)

        console.log(row);
        if (row === false) {
            await newStandardRequest(context['username'], requete)
        }
        else {
            await assignRequestToVip(row[0]['id'], requete, context.username)
        }
        msg = "Merci pour ta requete " + context['display-name']
        sendMessage(target, context, msg)
    } else { // Nothing to echo
        console.log(`* No request arguments`)
    }
}

function sl(target, context, params) {
    msg = "Vous pouvez trouver la songlist ici : " + slUrl
    sendMessage(target, context, msg)
}

//########################################################################
// Définitions des fonctions de reponses au chat
//########################################################################
// Helper function to send the correct type of message:
function sendMessage(target, context, message) {
    if (context['message-type'] === 'whisper') {
        client.whisper(target, message)
    } else {
        client.say(target, message)
    }
}



//########################################################################
// gestion des évenement de chat
//########################################################################

// Create a client with our options:
let client = new tmi.client(opts)

// Register our event handlers (defined below):
client.on('message', onMessageHandler)
client.on('connected', onConnectedHandler)
client.on('disconnected', onDisconnectedHandler)

// Connect to Twitch:
client.connect()

// Called every time a message comes in:
function onMessageHandler(target, context, msg, self) {
    if (self) { return } // Ignore messages from the bot

    // This isn't a command since it has no prefix:
    if (msg.substr(0, 1) !== commandPrefix) {
        console.log(`[${target} (${context['message-type']})] ${context.username}: ${msg}`)
        return
    }

    // Split the message into individual words:
    const parse = msg.slice(1).split(' ')
    // The command name is the first (0th) one:
    const commandName = parse[0]
    // The rest (if any) are the parameters:
    const params = parse.splice(1)

    // If the command is known, let's execute it:
    if (commandName in knownCommands) {
        // Retrieve the function by its name:
        const command = knownCommands[commandName]
        // Then call the command with parameters:
        command(target, context, params)
        console.log(`* Executed ${commandName} command for ${context.username}`)
    } else {
        console.log(`* Unknown command ${commandName} from ${context.username}`)
    }
}

// Called every time the bot connects to Twitch chat:
function onConnectedHandler(addr, port) {
    console.log(`* Connected to ${addr}:${port}`)
}

// Called every time the bot disconnects from Twitch:
function onDisconnectedHandler(reason) {
    console.log(`Womp womp, disconnected: ${reason}`)
    process.exit(1)
}

