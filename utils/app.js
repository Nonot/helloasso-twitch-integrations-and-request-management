const fetch = require("node-fetch");
const datas = require('../config/config.js')
const paramsHelloAsso = datas['helloasso']
const authlogin = paramsHelloAsso['username'] + ':' + paramsHelloAsso['key']
const campaignId = paramsHelloAsso['campaignid']
// Define database configuration:
const paramsDB = datas['db']
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: paramsDB['host'],
        user: paramsDB['user'],
        password: paramsDB['password'],
        database: paramsDB['database']
    }
});

async function name() {
    let resources = []
    let response
    let page = 1
    let a = true
    while (a) {
        let replast = await fetch('https://' + authlogin + '@api.helloasso.com/v3/campaigns/' + campaignId + `/actions.json?from=2017-01-01T00:00:00&results_per_page=1000&page=${page}`)
        response = await replast.json()
        resources.push(response['resources'])
        if (response['pagination']['page'] === response['pagination']['max_page']) {
            a = false
        }
        else {
            page++
        }
    }
    await knex('donateurs').truncate();
    let pseudo
    // console.log(resources);
    for (let i = 0; i < resources.length; i++) {
        const element = resources[i];
        for (let j = 0; j < element.length; j++) {
            const action = element[j];
            console.log(action['email'] + ' ' +action['first_name']);  
            if (action['custom_infos'][0] !== undefined) {
                if (action['custom_infos'][0]['value'] !== undefined) {
                    pseudo = action['custom_infos'][0]['value']
                }
            }

            try {
                await knex('donateurs').insert({ email: action['email'], prenom : action['first_name'], nom : action['last_name'], pseudo, montant:action['amount'], adresse : action[''] })
            }
            catch (err) {
                // console.log(err);
            }
        }
    }
    let doublons = await knex.raw('select *, count(nom) from donateurs group by nom having count(nom) > 1;')
    console.log(doublons[0]);
    process.exit()

}

name()
