

var socket = io.connect();

// Et on l'envoie avec le signal "petit_nouveau" (pour le différencier de "message")

socket.on('message', function (obj) {
    console.log(obj);
    
    if (typeof obj === "object") {
        var texte = obj['funding'] + "&nbsp;€";
        document.getElementById("funding").innerHTML = texte;
        document.getElementById("dernierDonateur").innerHTML = obj['lastName'];
        document.getElementById("dernierMontant").innerHTML = obj['lastAmount'] + "&nbsp;€";
        if (obj["sound"]) {
            console.log("bruit");
            playSound();
        }
    }
})
function playSound() {
    var audio = new Audio('audio_file.mp3');
    audio.play();
}
