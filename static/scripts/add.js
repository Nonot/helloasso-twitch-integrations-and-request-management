var socket = io.connect();

socket.on('newRequest', function (obj) {
    console.log(obj);
    id = obj['id']
    pseudo = obj['pseudo']
    requete = obj['requete']
    table = obj['table'] == 'vip'
        ? 'tabloVIP'
        : 'tabloSTD'
    amount = obj['montant']
    addRow(table, pseudo, requete, id, amount)
})

socket.on('initLists', function (obj) {
    console.log(obj['vip']);
    for (let i = 0; i < obj['vip'].length; i++) {
        const element = obj['vip'][i];
        addRow('tabloVIP', element['pseudo'], element['requete'], element['id'], element['montant'])
    }
    for (let i = 0; i < obj['standard'].length; i++) {
        const element = obj['standard'][i];
        addRow('tabloSTD', element['pseudo'], element['requete'], element['id'])
    }
})

socket.on('serverDelRow', function (obj) {
    delRow(obj['id'], obj['table'])
})
socket.on('serverUpRow', function (obj) {
    console.log(obj);
    
    updateRow(obj['id'], obj['table'], obj['requete'])
})

socket.emit('requestConnection', 'hello')
function playSound() {
    var audio = new Audio('audio_file.mp3');
    audio.play();
}
function addRow(tablo, name, requete, id, montant) {
    console.log(montant);

    const table = document.getElementById(tablo);
    // Create an empty <tr> element and add it to the 1st position of the table:
    const row = table.insertRow(-1);
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    console.log(row);
    row.id = id + tablo
    const cell1 = row.insertCell();
    const cell2 = row.insertCell();
    const cell3 = row.insertCell();
    cell2.innerHTML = name;
    cell3.innerHTML = "<input type='text' onchange='sendUpdateRow(this, " + tablo + ","+id+") '>"
    const cell4 = row.insertCell();
    if (montant !== undefined) {
        cell4.innerHTML = montant+ '&nbsp;€'
    }


    const input = cell3.childNodes[0]
    input.value = requete

    cell1.innerHTML = "<input type='button' onclick='sendDeleteRow(this, " + tablo + ")' value='X' id='" + id + "' class='" + tablo + "'/>"
}

function sendDeleteRow(dom, tablo) {
    console.log(dom.id);

    dom.parentElement.parentElement.remove()
    console.log(tablo.id);
    socket.emit('clientDelRow', { id: dom.id, status: tablo.id })
}
function sendUpdateRow(input, tablo,id){
    socket.emit('clientUpRow', { id: id, status: tablo.id, requete: input.value })
    console.log(input.value, tablo.id, id);
}
function delRow(id, table) {
    const row = document.getElementById(id + table)
    row.remove()
    console.log(row);
}
function updateRow(id, table, requete){
    document.getElementById(id + table).childNodes[2].children[0].value = requete
}